#!/usr/bin/env bash

## Add your own PPAs to this list. This list installs the Adapta theme, Papirus icon theme, Dolphin emulator, and the updated Nvidia drivers
ppas=( tista/adapta papirus/papirus dolphin-emu/ppa graphics-drivers/ppa )

ubuntu () {
	echo "Installing dependencies for Signal and Sublime Text"
	## NON-PPA INSTALLATIONS
	#=======================#
	## Signal requires curl installed and Sublime Text needs apt-transport-https so we'll get those to set the stage for later
	sudo apt install curl wget apt-transport-https
	
	## Signal setup stuffs
	if ! [ -e /etc/apt/sources.list.d/signal-xenial.list ] ; then
		echo "Setting groundwork for Signal"
		wget -qO - https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
		echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee /etc/apt/sources.list.d/signal-xenial.list
	fi

	## Sublime Text setup stuff
	if ! [ -e /etc/apt/sources.list.d/sublime-text.list ] ; then
		echo "Setting groundwork for Sublime Text"
		wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
		echo "deb https://download.sublimetext.com/ apt/dev/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	fi

	## Lutris setup stuffs
	if ! [ -e /etc/apt/sources.list.d/lutris.list ] ; then
		echo "Setting groundwork for Lutris"
		ver=$(lsb_release -sr); if [ $ver != "18.04" -a $ver != "17.10" -a $ver != "17.04" -a $ver != "16.04" ]; then ver=18.04; fi
		wget -qO - http://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/Release.key | sudo apt-key add -
		echo "deb http://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/ ./" | sudo tee /etc/apt/sources.list.d/lutris.list
	fi

	## Brave setup settings
	if ! [ -e /etc/apt/sources.list.d/brave-browser.list ] ; then
		wget -qO - https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | sudo apt-key add -
		echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt `lsb_release -sc` main" | sudo tee /etc/apt/sources.list.d/brave-browser.list
	fi

	## ***NOT COMPLETELY TESTED!! USE AT YOUR OWN RISK!!***
	if ! [ -e /etc/apt/sources.list.d/wine-staging.list ] ; then
		echo "Setting groundwork for Wine Staging"
		wget -qO - https://dl.winehq.org/wine-builds/Release.key | sudo apt-key add -
		echo "deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main" | sudo tee /etc/apt/sources.list.d/wine-staging.list
	fi


	echo "Installing PPAs"
	## PPA INSTALLATIONS
	#=======================#
	for i in ${ppas[@]}
	do
		sudo add-apt-repository ppa:$i
	done

	echo "Installing Signal, Adapta, Papirus, Dolphin, Sublime Text, and Lutris"
	## INSTALLING 
	#=======================#
	sudo apt install -y signal-desktop adapta-gtk-theme papirus-icon-theme dolphin-emu-master sublime-text lutris brave wine-staging winehq-staging
}

debian () {
	echo "Here is some Debian stuff..."
	sudo apt install -y steam 0ad gimp openshot openmw vlc mpv fail2ban deluge kdeconnect kdenlive golang obs-studio git handbrake libdvd-pkg 
}

#fedora () {

	## Sublime Text setup stuffs
	#sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
	#sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/dev/x86_64/sublime-text.repo

#}

#centOS () {}

#arch () {

	## Sublime Text setup stuffs
	#curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
	#echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/dev/x86_64" | sudo tee -a /etc/pacman.conf
#}

#openSUSE () {}
echo "Welcome to the reinstall script!"
echo "================================"

if [ -e /usr/bin/dnf ] ; then
	echo "You're running a Fedora based distro!"
fi

if [ -e /usr/bin/pacman ] ; then
	echo "You're running an Arch based distro!"
fi

if [ -e /usr/bin/zypper ] ; then
	echo "You're running an OpenSUSE based distro!"
fi

if [ -e /usr/bin/apt ] ; then
	echo "You're running a Debian based distro!"
	if [ -e /usr/bin/add-apt-repository ] ; then
		ubuntu
	fi
	debian
	
fi
