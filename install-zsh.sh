#!/usr/bin/env bash

#############################################
# A new linux computer setup script         #
#					   						#
# After getting tired of making all the     #
# customization tweaks after reinstalling   #
# Linux on every computer, I decided to     #
# automate the process with this Bash file. #
#					   						#
# This file won't work for everyone, as my  #
# customization tastes are different from   #
# others', but feel free to fork it and 	#
# add your own customizations!				#
#					   						#
# David Seward <davidseward@keemail.me>		#
# https://gitlab.com/RiderExMachina 		#
#############################################

# First thing is set the ZSH information.

# My .zshrc has everything already preconfigured
# so I just have to copy it and the necessary 
# files into the home directory
echo "Setting ZSH information"
ln -s $PWD/files/.zshrc /home/$USER/.zshrc
ln -s $PWD/files/.aliasrc /home/$USER/.aliasrc
mkdir zsh && cd zsh

# I then have to install Oh My ZSH and the special fonts
echo "Installing Oh My ZSH"
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
echo "Installed Oh My ZSH"

echo "Installing patched fonts"
git clone https://github.com/powerline/fonts.git --depth=1
fonts/install.sh
echo "Installed fonts"

# And then to finish it off I have to change the default shell
sudo chsh -s $(which zsh) $USER
echo "Shell changed, changes will not take effect until session is restarted"
echo "(Logging out and back in will suffice)"

